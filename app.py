from flask import Flask

app = Flask(__name__)

@app.route("/") #Sends response when this url path matches
#@app.route("/home")
def hello_world():
    return "Hello World"

@app.route("/home")
def somethin():
    return "Trying"
if __name__ == "__main__":
    app.run()
